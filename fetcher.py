import csv
import shutil
from collector import YahooCollector
from tempfile import NamedTemporaryFile

tempfile = NamedTemporaryFile(delete=False)

yahoo = YahooCollector()
yahoo.setBounds('1994', '2016')

with open('./sets/tech_companies.csv', 'r') as csvfile:
    header = csv.Sniffer().has_header(csvfile.read(1024))
    csvfile.seek(0)
    the_csv = csv.reader(csvfile)
    if header:
        next(the_csv)

    data = {}
    for row in the_csv:
        data[row[0]] = row[1]

    for key in data:
        key = key.strip()
        yahoo.setTicker(key)
        outfile = './prices/' + key + '.csv'
        yahoo.setOutFile(outfile)
        yahoo.fetch()

        with open(outfile, 'rw') as current_file:
            reader = csv.reader(current_file, delimiter=",")
            writer = csv.writer(tempfile, delimiter=",")
            for row in reader:
                row.append(key)
                writer.writerow(row)

        shutil.move(tempfile.name, outfile)


