import requests
import csv

original_set = './sets/tech_companies.csv'
modified_set = original_set.replace('.csv', '_with_ciks.csv')

modified_file = open(modified_set, 'w')
writer = csv.writer(modified_file, delimiter=',')

with open(original_set, 'r') as csvfile:
    has_header = csv.Sniffer().has_header(csvfile.read(1024))
    csvfile.seek(0)
    the_csv = csv.reader(csvfile)
    if has_header:
        row = next(the_csv)
        row.append('CIK Code')
        writer.writerow(row)

    for row in the_csv:
        ticker = row[0]
        string_match = 'rel="alternate"'
        parameters = {
            'company': '',
            'match': '',
            'CIK': ticker,
            'owner': 'exclude',
            'Find': 'Find Companies',
            'count': 1,
            'action': 'getcompany'
        }
        r = requests.get('http://www.sec.gov/cgi-bin/browse-edgar', params=parameters)

        print 'Working on ticker: {}'.format(ticker)

        for line in r.iter_lines():
            if string_match in line:
                for string in line.split(';'):
                    if 'CIK=' in string:
                        cik = string.strip().replace('CIK=', '').replace('&amp', '')
                        row.append(cik)
                        writer.writerow(row)
