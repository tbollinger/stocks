import requests
from bs4 import BeautifulSoup

cik = 320193
lpad_cik = str(cik).zfill(10)

parameters = {
  'company': '',
  'match': '',
  'CIK': lpad_cik,
  'owner': 'exclude',
  'Find': 'Find Companies',
  'count': 100,
  'action': 'getcompany'
}
r = requests.get('http://www.sec.gov/cgi-bin/browse-edgar', params=parameters)

print 'Working on ticker: {}'.format(lpad_cik)

soup = BeautifulSoup(r.content)
samples = soup.find_all("a")

links = []

for link in samples:
    if u'\xa0Documents' in link.contents:
        links.append(link['href'])

rnew = requests.get('http://www.sec.gov/Archives/edgar/data/320193/000119312514154871/0001193125-14-154871-index.htm')

soup2 = BeautifulSoup(rnew.content)
dox = soup2.find_all('a')

print dox