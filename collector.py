from pprint import pprint
import requests


class URLCollector(object):
    start = ''
    end = ''
    url = ''
    ticker = ''
    file_type = 'csv'
    out_file = ''
    parameters = {}
    accepted_file_types = ['csv', 'xml']

    def setFileType(self, file_type):
        if isinstance(file_type, str):
            if file_type in self.accepted_file_types:
                self.file_type = file_type
            else:
                raise Exception('file type not in list of accepted file types')
        else:
            raise Exception('setFileType requires a variable of type "str" to be passed')

    def setTicker(self, ticker):
        if isinstance(ticker, str):
            self.ticker = ticker
        else:
            raise Exception('setTicker requires a variable of type "str" to be passed')

    def getUrl(self):
        return self.url

    def setParams(self, parameters):
        if isinstance(parameters, dict):
            for k, v in parameters.iteritems():
                self.parameters[k] = v
        else:
            raise Exception('setParams requires a variable of type "dict" to be passed')

    def setBounds(self, start, end):
        if isinstance(start, str) and isinstance(end, str):
            self.setStart(start)
            self.setEnd(end)
        else:
            raise Exception('setBounds requires a variable of type "dict" to be passed')

    def setStart(self, start):
        if isinstance(start, str):
            self.start = start
        else:
            raise Exception('setStart requires a variable of type "str" to be passed')

    def setEnd(self, end):
        if isinstance(end, str):
            self.end = end
        else:
            raise Exception('setEnd requires a variable of type "str" to be passed')

    def setOutFile(self, out_file):
        self.out_file = out_file

    def fetch(self):
        if not self.ticker:
            raise Exception('No ticker defined!')
        else:
            self.bindTicker()

        if not self.start:
            raise Exception('No start date defined!')
        else:
            self.bindBounds()

        r = requests.get(self.url, params=self.parameters)
        if len(self.out_file) > 0:
            with open(self.out_file, 'w') as f:
                for line in r.text:
                    f.write(line)
        else:
            for line in r.text:
                print line


class YahooCollector(URLCollector):
    def __init__(self):
        params = {
            # 'a': 3,
            # 'b': 12,
            # 'd': 0,
            # 'e': 28,
            # 'g': 'd',
            'ignore': 'csv'
        }
        self.url = 'http://ichart.finance.yahoo.com/table.csv?'
        self.setParams(params)

    def bindTicker(self):
        self.setParams({'s': self.ticker})

    def bindBounds(self):
        self.setParams({'f': self.end, 'c': self.start})

        # self.hosts = {
        #     'yahoo': http://ichart.finance.yahoo.com/table.csv?s=YHOO&d=0&e=28&f=2010&g=d&a=3&b=12&c=1996&ignore=.csv
        # }

class CommentLetterCollector(URLCollector):
    def __init__(self):
        params = {
            'action': 'getcompany',
            'CIK': '',
            'type': 'UPLOAD',
            'dateb': '',
            'owner': 'exclude',
            'count': 100
        }
        self.url = 'https://www.sec.gov/cgi-bin/browse-edgar'
        self.setParams(params)


# Using Yahoo's CSV approach above you can also get historical data! You can reverse engineer the following example:
#
# http://ichart.finance.yahoo.com/table.csv?s=YHOO&d=0&e=28&f=2010&g=d&a=3&b=12&c=1996&ignore=.csv
#
# Essentially:
#
# sn = TICKER
# a = fromMonth-1
# b = fromDay (two digits)
# c = fromYear
# d = toMonth-1
# e = toDay (two digits)
# f = toYear
# g = d for day, m for month, y for yearly
# The complete list of parameters:
#
# a   Ask
# a2  Average Daily Volume
# a5  Ask Size
# b   Bid
# b2  Ask (Real-time)
# b3  Bid (Real-time)
# b4  Book Value
# b6  Bid Size
# c   Change & Percent Change
# c1  Change
# c3  Commission
# c6  Change (Real-time)
# c8  After Hours Change (Real-time)
# d   Dividend/Share
# d1  Last Trade Date
# d2  Trade Date
# e   Earnings/Share
# e1  Error Indication (returned for symbol changed / invalid)
# e7  EPS Estimate Current Year
# e8  EPS Estimate Next Year
# e9  EPS Estimate Next Quarter
# f6  Float Shares
# g   Day's Low
# h   Day's High
# j   52-week Low
# k   52-week High
# g1  Holdings Gain Percent
# g3  Annualized Gain
# g4  Holdings Gain
# g5  Holdings Gain Percent (Real-time)
# g6  Holdings Gain (Real-time)
# i   More Info
# i5  Order Book (Real-time)
# j1  Market Capitalization
# j3  Market Cap (Real-time)
# j4  EBITDA
# j5  Change From 52-week Low
# j6  Percent Change From 52-week Low
# k1  Last Trade (Real-time) With Time
# k2  Change Percent (Real-time)
# k3  Last Trade Size
# k4  Change From 52-week High
# k5  Percent Change From 52-week High
# l   Last Trade (With Time)
# l1  Last Trade (Price Only)
# l2  High Limit
# l3  Low Limit
# m   Day's Range
# m2  Day's Range (Real-time)
# m3  50-day Moving Average
# m4  200-day Moving Average
# m5  Change From 200-day Moving Average
# m6  Percent Change From 200-day Moving Average
# m7  Change From 50-day Moving Average
# m8  Percent Change From 50-day Moving Average
# n   Name
# n4  Notes
# o   Open
# p   Previous Close
# p1  Price Paid
# p2  Change in Percent
# p5  Price/Sales
# p6  Price/Book
# q   Ex-Dividend Date
# r   P/E Ratio
# r1  Dividend Pay Date
# r2  P/E Ratio (Real-time)
# r5  PEG Ratio
# r6  Price/EPS Estimate Current Year
# r7  Price/EPS Estimate Next Year
# s   Symbol
# s1  Shares Owned
# s7  Short Ratio
# t1  Last Trade Time
# t6  Trade Links
# t7  Ticker Trend
# t8  1 yr Target Price
# v   Volume
# v1  Holdings Value
# v7  Holdings Value (Real-time)
# w   52-week Range
# w1  Day's Value Change
# w4  Day's Value Change (Real-time)
# x   Stock Exchange