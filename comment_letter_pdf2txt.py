import subprocess
from tempfile import NamedTemporaryFile
import shutil

pdfFile = './pdfs/filename1.pdf'
txtFile = './txts/filename1.txt'

tempfile = NamedTemporaryFile(delete=False)

with open(tempfile.name, 'w') as f:
    subprocess.Popen(["pdftotext", pdfFile, '-'], stdout=f)
    shutil.move(tempfile.name, txtFile)


